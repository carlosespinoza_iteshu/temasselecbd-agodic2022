﻿namespace DALMongo
{
    public class Dispositivo
    {
        public string Id { get; set; }
        public string Ubicacion { get; set; }
        public string Modelo { get; set; }
        public DateTime FechaColocacion { get; set; }
    }
}
