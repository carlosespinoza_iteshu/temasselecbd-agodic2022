﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALMongo
{
    public class RepositoryDispositivos
    {
        MongoClient cliente;
        IMongoDatabase db;

        public RepositoryDispositivos()
        {
            cliente = new MongoClient("mongodb+srv://aurora:plZFU0tmyXMYD2Dw@cluster0.d3nn6h8.mongodb.net/?retryWrites=true&w=majority");
            db = cliente.GetDatabase("Lecturas01");
        }

        private IMongoCollection<Dispositivo> coleccion() => db.GetCollection<Dispositivo>("Dispositivo");

        public IEnumerable<Dispositivo> ObtenerTodos
        {
            get
            {
                return coleccion().AsQueryable();
            }
        }

        public IEnumerable<Dispositivo> TraerPorFecha(int mes, int anio)
        {
            return coleccion().Find(d => d.FechaColocacion.Year == anio && d.FechaColocacion.Month == mes).ToList();
        }

        public Dispositivo ObtenerDispositivo(string id)
        {
            //select * from Dispositivo where Id='';
            return coleccion().Find(d => d.Id == id).SingleOrDefault();
        }

        public Dispositivo ObtenerDispositivoPorUbicacion(string ubicacion)
        {
            return coleccion().Find(d => d.Ubicacion == ubicacion).SingleOrDefault();
        }

        public Dispositivo Insertar(Dispositivo value)
        {
            value.Id = Guid.NewGuid().ToString();
            coleccion().InsertOne(value);
            return value;
        }

        public Dispositivo Modificar(string id, Dispositivo value)
        {
            coleccion().ReplaceOne(d => d.Id == id, value);
            return value;
        }

        public bool Eliminar(string id)
        {
            try
            {
                coleccion().DeleteOne(d => d.Id == id);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
