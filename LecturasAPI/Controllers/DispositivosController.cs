﻿using DALMongo;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace LecturasAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DispositivosController : ControllerBase
    {
        private RepositoryDispositivos repository;
        public DispositivosController()
        {
            repository = new RepositoryDispositivos();

        }


        // GET: api/<DispositivosController>
        [HttpGet]
        public IEnumerable<Dispositivo> Get()
        {
            //select * from Dispositivo
            return repository.ObtenerTodos;
        }
        [HttpGet("TraerPorFecha/{mes}/{anio}")]
        public IEnumerable<Dispositivo> TraerPorFecha(int mes, int anio)
        {
            return repository.TraerPorFecha(mes, anio);
        }

        // GET api/<DispositivosController>/5
        [HttpGet("{id}")]
        public Dispositivo Get(string id)
        {
            //select * from Dispositivo where Id='';
            return repository.ObtenerDispositivo(id);
        }

        // POST api/<DispositivosController>
        [HttpPost]
        public Dispositivo Post([FromBody] Dispositivo value)
        {
            return repository.Insertar(value);
        }

        // PUT api/<DispositivosController>/5
        [HttpPut("{id}")]
        public Dispositivo Put(string id, [FromBody] Dispositivo value)
        {
            return repository.Modificar(id, value);
        }

        // DELETE api/<DispositivosController>/5
        [HttpDelete("{id}")]
        public bool Delete(string id)
        {
            return repository.Eliminar(id);
        }
    }
}
