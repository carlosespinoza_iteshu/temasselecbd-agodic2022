﻿using DALInflux;
using InfluxDB.Client;
using InfluxDB.Client.Writes;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace LecturasAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LecturasController : ControllerBase
    {
        RepositorioLecturas repositorio;
        public LecturasController()
        {
            repositorio = new RepositorioLecturas();
        }

        [HttpPost]
        public ActionResult<bool> POST([FromBody] Lectura lectura)
        {
            try
            {
                var r = repositorio.Insertar(lectura);
                if (r!=null)
                {
                    return Ok(r);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpGet("{ubicacion}/{elemento}")]
        public ActionResult<DatoLectura> GET(string ubicacion, string elemento)
        {
            try
            {
                return Ok(repositorio.ObtenerElementoDeUbicacion(ubicacion, elemento));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{inicio}/{fin}/{ubicacion}/{elemento}")]
        public ActionResult<List<DatoLectura>> GET(DateTime inicio, DateTime fin, string ubicacion="", string elemento="")
        {
            try
            {
                return Ok(repositorio.ObtenerElementosEnIntervalo(inicio,fin,ubicacion,elemento));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("UltimosValores")]
        public ActionResult<List<DatoLectura>> UltimosValores()
        {
            try
            {
                return Ok(repositorio.ObtenerUltimosValores());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
                throw;
            }
        }
    }
}
