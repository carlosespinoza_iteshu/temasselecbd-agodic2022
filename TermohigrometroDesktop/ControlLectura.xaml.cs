﻿using DALInflux;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace TermohigrometroDesktop
{
    /// <summary>
    /// Lógica de interacción para ControlLectura.xaml
    /// </summary>
    public partial class ControlLectura : UserControl
    {
        DispatcherTimer timer;
        RepositorioLecturas repositorioLecturas;
        string ubicacion;
        public ControlLectura(string ubicacion, RepositorioLecturas repositorio, int segundosActualizacion)
        {
            InitializeComponent();
            repositorioLecturas = repositorio;
            this.ubicacion = ubicacion;
            timer = new DispatcherTimer();
            timer.Tick += Timer_Tick;
            timer.Interval = new TimeSpan(0, 0, segundosActualizacion);
            timer.Start();
            Timer_Tick(null, null);
        }

        private void Timer_Tick(object? sender, EventArgs e)
        {
            var temp = repositorioLecturas.ObtenerUltimosValores(ubicacion, "Temperatura");
            var hum = repositorioLecturas.ObtenerUltimosValores(ubicacion, "Humedad");
            if (temp != null && hum != null)
            {
                AsignaValores(ubicacion, hum.Valor, temp.Valor, temp.FechaHora,false);
            }
            else
            {
                AsignaValores(ubicacion, 0, 0, DateTime.Now,true);
            }
        }

        private void AsignaValores(string ubicacion, int humedad, int temperatura, DateTime fechahora,bool sinDatos)
        {
            lblUbicacion.Content = ubicacion;
            lblFechaHora.Content = fechahora.ToString();
            valorHumedad.Value = humedad;
            valorTemperatura.Value = temperatura;
            if (!sinDatos)
            {
                gaugeTemperatura.GaugeHeader = $"Temperatura {temperatura}°C";
                gaugeHumedad.GaugeHeader = $"Humedad {humedad}%";
            }
            else
            {
                gaugeTemperatura.GaugeHeader = $"Sin datos";
                gaugeHumedad.GaugeHeader = $"Sin datos";
            }
        }
    }
}
