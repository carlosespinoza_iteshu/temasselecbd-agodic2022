﻿using InfluxDB.Client;
using InfluxDB.Client.Api.Domain;
using InfluxDB.Client.Writes;
using NodaTime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALInflux
{
    public class RepositorioLecturas
    {
        InfluxDBClient cliente;
        DateTimeZone systemZone = DateTimeZoneProviders.Tzdb.GetSystemDefault();
        const string bucket = "Lecturas02";
        const string org = "cespinoza@iteshu.edu.mx";
        const string token = "KwQ-L4Z9aI8najjxB1s2SdP9pQy3aYG6hJzfwz7rUIyXkUbPzfn3MMzs8rEc2NJnXb8IOj9O362tbMBLUVy5UA==";
        const string url = "https://us-east-1-1.aws.cloud2.influxdata.com";
        const string measurement = "Termohigrometro";
        public string Error { get;private set; }
        public RepositorioLecturas()
        {
            Error = "";
            cliente = InfluxDBClientFactory.Create(url, token);
        }

        public Lectura Insertar(Lectura lectura)
        {
            try
            {
                Error = "";
                var punto = PointData
                .Measurement(measurement)
                .Tag("Ubicacion", lectura.Ubicacion)
                .Field("Temperatura", lectura.Temperatura)
                .Field("Humedad",lectura.Humedad)
                .Timestamp(lectura.FechaHora.ToUniversalTime(), InfluxDB.Client.Api.Domain.WritePrecision.Ns);
                using (var writeapi = cliente.GetWriteApi())
                {
                    writeapi.WritePoint(punto,bucket,org);
                }
                return lectura; 
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }

        private async Task<IEnumerable<DatoLectura>> EjecutaConsulta(string consula)
        {
            List<DatoLectura> datos = new List<DatoLectura>();
            var result = await cliente.GetQueryApi().QueryAsync(consula, org).ConfigureAwait(false);
            result.ForEach(tabla =>
            {
                var data = tabla.Records;
                data.ForEach(data =>
                {
                    datos.Add(new DatoLectura(data.GetTimeInDateTime().Value.AddHours(-6),data.GetMeasurement(), data.GetValueByKey("Ubicacion").ToString(), data.GetField(), data.GetValue().ToString()));
                });
            });
            return datos;
        }


        public IEnumerable<DatoLectura> ObtenerElementoDeUbicacion(string ubicacion, string elemento)
        {
            string query = $"from(bucket: \"{bucket}\") |> range(start: 0) |> filter(fn: (r) => r[\"_measurement\"] == \"Termohigrometro\") |> filter(fn: (r) => r[\"Ubicacion\"] == \"{ubicacion}\") |> filter(fn: (r) => r[\"_field\"] == \"{elemento}\")";
            return EjecutaConsulta(query).Result;
        }

        public IEnumerable<DatoLectura> ObtenerElementosEnIntervalo(DateTime inicio, DateTime fin, string ubicacion="", string elemento = "")
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine($"from(bucket: \"{bucket}\")");
            query.AppendLine($"|> range(start: {inicio.ToUniversalTime().ToString("yyyy-MM-dd'T'HH:mm:ss'Z'")} , stop: {fin.ToUniversalTime().ToString("yyyy-MM-dd'T'HH:mm:ss'Z'")})");
            query.AppendLine($"|> filter(fn: (r) => r[\"_measurement\"] == \"{measurement}\")");
            if(!string.IsNullOrEmpty(ubicacion))query.AppendLine($"|> filter(fn: (r) => r[\"Ubicacion\"] == \"{ubicacion}\")");
            if(!string.IsNullOrEmpty(elemento))query.AppendLine($"|> filter(fn: (r) => r[\"_field\"] == \"{elemento}\")");
            return EjecutaConsulta(query.ToString()).Result;
        }

        public IEnumerable<DatoLectura> ObtenerUltimosValores()
        {
            string query = $"from(bucket: \"{bucket}\")\r\n  |> range(start: 0)\r\n  |> filter(fn: (r) => r[\"_measurement\"] == \"{measurement}\")\r\n  |> filter(fn: (r) => r[\"_field\"] == \"Humedad\" or r[\"_field\"] == \"Temperatura\")\r\n  |> last()";
            return EjecutaConsulta(query).Result;
        }

        public DatoLectura ObtenerUltimosValores(string ubicacion, string elemento)
        {
            string query = $"from(bucket: \"{bucket}\")\r\n  |> range(start: 0)\r\n  |> filter(fn: (r) => r[\"_measurement\"] == \"{measurement}\")\r\n  |> filter(fn: (r) => r[\"_field\"] == \"{elemento}\")\r\n  |> filter(fn: (r) => r[\"Ubicacion\"] == \"{ubicacion}\") \r\n |> last()";
            return EjecutaConsulta(query).Result.SingleOrDefault();

        }
    }
}
