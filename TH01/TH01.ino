/*
 Name:		TH01.ino
 Created:	08/11/2022 10:28:42
 Author:	MIDS. Carlos Espinoza
*/

#include <WiFi.h>
#include <DHTesp.h>
#include <HTTPClient.h>
#include <InfluxDbClient.h>
#include <InfluxDbCloud.h>
#define INFLUXDB_URL "https://us-east-1-1.aws.cloud2.influxdata.com"
#define INFLUXDB_TOKEN "KwQ-L4Z9aI8najjxB1s2SdP9pQy3aYG6hJzfwz7rUIyXkUbPzfn3MMzs8rEc2NJnXb8IOj9O362tbMBLUVy5UA=="
#define INFLUXDB_ORG "cespinoza@iteshu.edu.mx"
#define INFLUXDB_BUCKET "Lecturas02"
#define TZ_INFO "UTC-6" //Depende de donde se localize fisicamente nuestro dispositivo
String ssid = "Zero's WiFi";
String pass = "qwertyuiop99";
String serverName = "http://www.apilecturas.somee.com/api/Lecturas";
String medicion = "Termohigrometro";
String ubicacion = "Cubiculo Profe Carlos";
InfluxDBClient client(INFLUXDB_URL, INFLUXDB_ORG, INFLUXDB_BUCKET, INFLUXDB_TOKEN, InfluxDbCloud2CACert);
Point sensorDB(medicion);
#define DHTpin 4
DHTesp sensor;
WiFiClient wifi;
int temp, hum;
void setup() {
    Serial.begin(9600);
    sensor.setup(DHTpin, DHTesp::DHT11);
    ConectarWiFi();
}

void CargarDirecto() {
    sensorDB.clearFields();
    sensorDB.addTag("ubicacion", ubicacion);
    sensorDB.addField("Temperatura", temp);
    sensorDB.addField("Humedad", hum);
    if (!client.writePoint(sensorDB)) {
        Serial.print("InfluxDB write failed: ");
        Serial.println(client.getLastErrorMessage());
    }
    else {
        Serial.println("Dato almacenado en InfluxDB");
    }
}

void loop() {
    Sensar();
    CargarDirecto();
    delay(10000);
}

void Sensar() {
    TempAndHumidity valores = sensor.getTempAndHumidity();
    temp = valores.temperature;
    hum = valores.humidity;
    Serial.println("T=" + String(temp) + " H=" + String(hum));
}

void ConectarWiFi() {
    WiFi.begin(ssid.c_str(), pass.c_str());
    Serial.print("Conectando a WiFi");
    while (WiFi.status() != WL_CONNECTED) {
        Serial.print(".");
        delay(500);
    }
    Serial.println(" Ok!!");
    timeSync(TZ_INFO, "pool.ntp.org", "time.nis.gov");
    if (client.validateConnection()) {
        Serial.print("Connected to InfluxDB: ");
        Serial.println(client.getServerUrl());
    }
    else {
        Serial.print("InfluxDB connection failed: ");
        Serial.println(client.getLastErrorMessage());
    }
}
