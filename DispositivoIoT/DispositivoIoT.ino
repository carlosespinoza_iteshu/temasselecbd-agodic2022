#include <WiFi.h>
#include <DHTesp.h>
#include <HTTPClient.h>
#include <InfluxDbClient.h>
#include <InfluxDbCloud.h>
#define INFLUXDB_URL "https://us-east-1-1.aws.cloud2.influxdata.com"
#define INFLUXDB_TOKEN "5LryrSTGQEnRD2hj7SKeuwNfj7KSoZcXvdxv74RZCLLzWlWlyl9gk344k-1mfPrXquCHKAc1sxPd46qG3Kcd9Q=="
#define INFLUXDB_ORG "886344decbefd86f"
#define INFLUXDB_BUCKET "lecturas"
#define TZ_INFO "UTC-5" //Depende de donde se localize fisicamente nuestro dispositivo
String ssid = "Zero's WiFi";
String pass = "qwertyuiop99";
String serverName = "http://www.apilecturas.somee.com/api/Lecturas";
InfluxDBClient client(INFLUXDB_URL, INFLUXDB_ORG, INFLUXDB_BUCKET, INFLUXDB_TOKEN, InfluxDbCloud2CACert);
Point sensorDB("profeCarlos");
#define DHTpin 4
DHTesp sensor;
WiFiClient wifi;
int temp, hum;
void setup() {
  Serial.begin(9600);
  sensor.setup(DHTpin, DHTesp::DHT11);
  ConectarWiFi();
}
void Sensar() {
  TempAndHumidity valores = sensor.getTempAndHumidity();
  temp = valores.temperature;
  hum = valores.humidity;
  Serial.println("T=" + String(temp) + " H=" + String(hum));
}

void ConectarWiFi(){
  WiFi.begin(ssid.c_str(), pass.c_str());
  Serial.print("Conectando a WiFi");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");    
    delay(500);
  }
  Serial.println(" Ok!!");
  timeSync(TZ_INFO, "pool.ntp.org", "time.nis.gov");
  if (client.validateConnection()) {
      Serial.print("Connected to InfluxDB: ");
      Serial.println(client.getServerUrl());
    } else {
      Serial.print("InfluxDB connection failed: ");
      Serial.println(client.getLastErrorMessage());
    }
}

void CargarDatosPorAPI(){
  String json="{\"id\": \"string\",\"idDispositivo\": \"string\",\"fechaHora\": \"2022-10-29T15:53:52.929Z\",\"temperatura\": " + String(temp) + ",\"humedad\": " + String(hum) + "}";
  HTTPClient http;
  http.begin(wifi, serverName);
  http.addHeader("Content-Type","application/json");
  int respuesta=http.POST(json);
  Serial.println("Respuesta: " + String(respuesta));
}

void CargarDirecto(){
  sensorDB.clearFields();
  sensorDB.addField("Temperatura", temp);
  sensorDB.addField("Humedad", hum);
  if (!client.writePoint(sensorDB)) {
      Serial.print("InfluxDB write failed: ");
      Serial.println(client.getLastErrorMessage());
    }else{
      Serial.println("Dato almacendo en InfluxDB");
    }
}

void loop() {
  Sensar();
  //CargarDatosPorAPI();
  CargarDirecto();
  delay(5000);

}
